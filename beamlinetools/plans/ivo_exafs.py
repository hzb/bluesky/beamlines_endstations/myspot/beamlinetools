import os
import yaml
import numpy as np
from bluesky.plans import list_scan
from bluesky.plan_stubs import move_per_step, trigger_and_read

BASE_PATH = '/opt/bluesky/user_scripts'

def read_yaml(last_folder, file_name):
    """Reads a YAML file from a specified folder and filename, and returns its content.

    Args:
        last_folder: A string specifying the subdirectory within the base path.
        file_name: A string specifying the name of the file, without the .yml extension.

    Returns:
        The loaded YAML data as a dictionary, or None if the file does not exist or an error occurs.
    """
    file_path = os.path.join(BASE_PATH, last_folder, f"{file_name}.yml")
    try:
        with open(file_path, 'r') as file:
            data = yaml.safe_load(file)
        return data
    except FileNotFoundError:
        print(f"Error: File '{file_name}.yml' not found in directory '{BASE_PATH}/{last_folder}'")
        return None
    except yaml.YAMLError as exc:
        print(f"Error: YAML syntax error in file '{file_name}.yml': {exc}")
        return None

def calculate_points(last_folder, file_name):
    """Calculates the energy points and detector exposure times based on a YAML configuration.

    Args:
        last_folder: A string specifying the folder where the YAML configuration file is located.
        file_name: A string specifying the name of the YAML file without the extension.

    Returns:
        A tuple containing two lists: energy_points and detector_exposure_times.
    """
    config_data = read_yaml(last_folder, file_name)
    if not config_data or 'edge' not in config_data or 'regions' not in config_data:
        print("Invalid or incomplete configuration data.")
        return [], []

    edge = config_data['edge']
    k_to_e = 262.4682917  # Conversion factor from k-space to energy
    energy_points_list, det_exp_list = [], []

    for region in config_data['regions']:
        start, stop, step, exp_times = process_region(region, edge, k_to_e)
        energy_points_list.extend(np.arange(start, stop, step))
        det_exp_list.extend(exp_times)

    return filter_smaller_values(energy_points_list, det_exp_list)

def process_region(region, edge, k_to_e):
    """Processes a single region from the configuration to calculate energy points and exposure times.

    Args:
        region: A dictionary containing the configuration for a single region.
        edge: The edge energy value.
        k_to_e: The conversion factor from k-space to energy.

    Returns:
        A tuple containing the start, stop, step values for energy points, and a list of exposure times.
    """
    if region['name'] != 'exafs':
        start = edge + region['pre']
        stop = edge + region['post']
        step = region['step']
        exp_times = np.full(int(np.ceil((stop - start) / step)), region['exp_time']).tolist()
    else:
        start = edge + (region['k_start'] ** 2) / k_to_e
        stop = edge + (region['k_stop'] ** 2) / k_to_e
        step = (region['k_step'] ** 2) / k_to_e
        # Calculates the total number of points including the last partial step 
        # by rounding up the division of the range by the step size.
        num_points = int(np.ceil((stop - start) / step))
        exp_times = np.linspace(region['min_exp_time'], region['max_exp_time'], num_points).tolist()

    return start, stop, step, exp_times

def filter_smaller_values(energy_points_list, det_exp_list):
    """Filters out energy points with values smaller than any previously seen value.

    Args:
        energy_points_list: A list of energy points.
        det_exp_list: A list of corresponding detector exposure times.

    Returns:
        Two lists: filtered_energy_points and filtered_det_exp, containing the filtered values.
    """
    filtered_energy_points, filtered_det_exp = [], []
    max_energy = 0

    for energy, det_exp in zip(energy_points_list, det_exp_list):
        if energy > max_energy:
            filtered_energy_points.append(energy)
            filtered_det_exp.append(det_exp)
            max_energy = energy

    return filtered_energy_points, filtered_det_exp

def exafs(detectors, motor, folder='examples', file='exafs', verbose=False, **kwargs):
    """Performs an EXAFS scan using the bluesky framework based on a given YAML configuration.

    The YAML file must specify an edge and a series of regions, with the last region being named 'exafs'.

    Args:
        detectors: A list of detector objects to use in the scan.
        motor: The motor object to move during the scan.
        folder: A string specifying the folder containing the YAML configuration.
        file: A string specifying the name of the YAML configuration file.
        verbose: A boolean indicating whether to print additional output during the scan.

    Example YAML configuration in 'user_script' folder:
        edge: 750
        regions:
            - name: 1
              pre: -0.06
              post: -0.04
              step: 0.02
              exp_time: 0.1
            - name: 2
              pre: -0.04
              post: 0.02
              step: 0.01
              exp_time: 0.2
            - name: 'exafs'
              k_start: 1
              k_stop: 13
              k_step: 1
              min_exp_time: .1
              max_exp_time: 5
    """
    energy_points, detector_exposure_times = calculate_points(folder, file)
    exposure = (exp_time for exp_time in detector_exposure_times)  # Generator expression
    if verbose:
        print("These is the list of points and detector exposures")
        for i in range(len(energy_points)):
            print(f"En:{energy_points[i]}, exp time:{detector_exposure_times[i]}")
    def one_nd_step_with_det_exposure(detectors, step, pos_cache):
        """Modified one_nd_step function to change the exposure time of the detector."""
        motors = step.keys()
        yield from move_per_step(step, pos_cache)
        exp = next(exposure)
        for d in detectors:
            # if verbose:
            #     print(f'exp time {exp}')
            if d.name == "mca":
                d.preset_real_time.set(exp)
            d.exposure_time = exp
        yield from trigger_and_read(list(detectors) + list(motors))

    kwargs.setdefault('per_step', one_nd_step_with_det_exposure)
    yield from list_scan(detectors, motor, energy_points, **kwargs)
