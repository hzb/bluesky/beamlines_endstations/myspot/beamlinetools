# from ophyd import ( Component as Cpt, ADComponent, Device, PseudoPositioner,
#                     EpicsSignal, EpicsSignalRO, EpicsMotor,
#                     ROIPlugin, ImagePlugin,
#                     SingleTrigger, PilatusDetector,
#                     OverlayPlugin, FilePlugin, EigerDetector,EigerDetectorCam, TIFFPlugin, TIFFPlugin, ProcessPlugin, StatsPlugin, ColorConvPlugin)
# from ophyd.areadetector.filestore_mixins import FileStoreTIFFIterativeWrite
# from ophyd.areadetector import EigerDetectorCam, AreaDetector
# from ophyd.areadetector.base import EpicsSignalWithRBV
# from bessyii.ad33 import SingleTriggerV33
# from ophyd.utils import set_and_wait
# from ophyd.areadetector.cam import AreaDetectorCam
# from ophyd.status import SubscriptionStatus
# import re

# class EigerDetectorCamV33(EigerDetectorCam):
#     '''This is used to update the Eiger detector to AD33.
#     '''
#     firmware_version = Cpt(EpicsSignalRO, 'FirmwareVersion_RBV', kind='config')

#     wait_for_plugins = Cpt(EpicsSignal, 'WaitForPlugins',
#                            string=True, kind='config')
    

#     def increment_number_in_string(self, s):
#         # Find the last number in the string
#         match = re.search(r'\d+$', s)
#         if match:
#             # Extract the number and increment it
#             number = int(match.group())
#             incremented_number = number + 1
#             # Replace the old number with the incremented number
#             new_s = re.sub(r'\d+$', str(incremented_number), s)
#             return new_s
#         else:
#             # Return the original string if no number is found at the end
#             return s


#     def stage(self, *args, **kwargs):
#         super().stage(*args, **kwargs)
#         set_and_wait(self.manual_trigger, 1)

#         #read the current filename 
#         current_file_name = self.fw_name_pattern.get()
#         new_file_name = self.increment_number_in_string(current_file_name)
#         #increment by 1
#         self.fw_name_pattern.set(new_file_name)
#         #arm
#         def check_value(*, old_value, value, **kwargs):
#             "Return True when the acquisition is complete, False otherwise."
#             return (old_value == 0 and value == 1)

#         status = SubscriptionStatus(self.armed, check_value, run=False)
#         self.acquire.set(1)
#         return status
    
#     def unstage(self, *args, **kwargs):
#         ret = super().unstage(*args, **kwargs)
#         # disable manual trigger
#         set_and_wait(self.manual_trigger, 0)
#         # disarm (by setting acq to 0)

#         def check_value(*, old_value, value, **kwargs):
#             "Return True when the acquisition is complete, False otherwise."
#             return (old_value == 1 and value == 0)

#         status = SubscriptionStatus(self.armed, check_value, run=False)

#         self.acquire.set(0)
#         return status

#     def trigger(self, *args, **kwargs):

#         def check_value(*, old_value, value, **kwargs):
#             "Return True when the acquisition is complete, False otherwise."
#             return (value - old_value == 1  )

#         status = SubscriptionStatus(self.num_images_counter, check_value, run=False)
        
#         set_and_wait(self.special_trigger_button, 1)

#         return status
    



#     def __init__(self, *args, **kwargs):
#         super().__init__(*args, **kwargs)
#         self.stage_sigs['wait_for_plugins'] = 'Yes'
#         self.stage_sigs['trigger_mode'] = 0
#         self.stage_sigs['num_triggers'] =1000

#     def ensure_nonblocking(self):
#         self.stage_sigs['wait_for_plugins'] = 'Yes'
#         for c in self.parent.component_names:
#             cpt = getattr(self.parent, c)
#             if cpt is self:
#                 continue
#             if hasattr(cpt, 'ensure_nonblocking'):
#                 cpt.ensure_nonblocking()

# class TIFFPluginWithFileStore(TIFFPlugin, FileStoreTIFFIterativeWrite):
#     ...


# class EigerDetector(EigerDetector):
    
#     cam = Cpt(EigerDetectorCamV33, 'cam1:')
    
#     def trigger(self):
       
#        # Call the cam trigger method (assumes that the device is already armed)
#        return  self.cam.trigger()
        
#     def stage(self, *args, **kwargs):
#         return self.cam.stage(*args, **kwargs)
    
#     def unstage(self, *args, **kwargs):
#         return self.cam.unstage(*args, **kwargs)
    
#     @property
#     def hints(self):
#         return {'fields': [self.stats1.total.name]}

#     def set_detector(self):

#         #self.tiff.kind = 'normal' 
#         #self.stats1.kind = 'hinted'
#         #self.image.kind = 'hinted'
#         #self.stats1.total.kind = 'hinted'
#         #self.stats1.centroid.x.kind = 'hintedew_' 
#         #self.stats1.centroid.y.kind = 'hinted' 
#         self.cam.ensure_nonblocking()









#     # def set_aquisition_time(self, value):
#     #     """The aquire time is in seconds"""
#     #     self.cam.acquire_time.set(value)
#     #     self.cam.acquire_period.set(value)
        

#     # def stage(self):
#     #     self.cam.fw_name_pattern.set("user/mySpot/2024-03-12_test/test") # EIGER_dir_name = ME[1]  # EIGER_file_name = ME[2]
#     #     self.cam.trigger_mode.set(3) # this set it to internal series, EIGER_trigger_mode = ME[4]det
#     #     self.cam.num_triggers.set(3) # This is now the number of images per file
#     #     self.cam.num_images.set(1) # EIGER_nimages = int(ME[5])
#     #     self.cam.fw_num_images_per_file.set(1) 
#     #     self.cam.photon_energy.set(8000) # we have to catch this automatically
#     #     self.cam.compression_algo.set(1) # set compression method to bslz4
#     #     self.cam.beam_center_y.set(1500)
#     #     self.cam.det_distance.set(300)
#     #     self.cam.acquire_time.set(1)
#     #     self.cam.acquire.set(1) # arm the detector, the detector is now waiting to be triggered
#     #     # I could not find this, not sure is needed. EIGER_image_start_nr = int(ME[3])
#     #     super().stage()

#     # def unstage(self):
#     #     self.cam.acquire.set(0) # disarm the detector
#     #     super().unstage()

#     # def trigger_to_be_written(self):
#     #     #Create a callback called if count is processed
#     #     def new_value(*,old_value,value,**kwargs):          #MDEL of $(P):rdCur must be set to -1

#     #         status.set_finished()
            
#     #         # Clear the subscription.
#     #         self.readback.clear_sub(new_value)

#     #     #Create the status object
#     #     status = DeviceStatus(self.readback,timeout = 10.0)

#     #     #Connect the callback that will set finished and clear sub
#     #     self.readback.subscribe(new_value,event_type=Signal.SUB_VALUE,run=False)
        

#     #     self.cam.special_trigger_button.set(1)  
        
#     #     return status

	    
# # - We need to send the parameters and arm the detector
# # - we need to send a software trigger at every measurements
# # - we have to be able to cancel and abort the scan
# #  - we disarm the detector at the end
# # - we have to be able to wait for the detector to end the acquisition or whatever it is doing. 



from ophyd import ( Component as Cpt, ADComponent, Device, PseudoPositioner,
                    EpicsSignal, EpicsSignalRO, EpicsMotor,
                    ROIPlugin, ImagePlugin,
                    SingleTrigger, PilatusDetector,
                    OverlayPlugin, FilePlugin, EigerDetector,EigerDetectorCam, TIFFPlugin, TIFFPlugin, ProcessPlugin, StatsPlugin, ColorConvPlugin)
from ophyd.areadetector.filestore_mixins import FileStoreTIFFIterativeWrite
from ophyd.areadetector import EigerDetectorCam, AreaDetector
from ophyd.areadetector.base import EpicsSignalWithRBV
from bessyii.ad33 import SingleTriggerV33
from ophyd.utils import set_and_wait
from ophyd.areadetector.cam import AreaDetectorCam
from ophyd.status import SubscriptionStatus
import re

class EigerDetectorCamV33(EigerDetectorCam):
    '''This is used to update the Eiger detector to AD33.
    '''
    firmware_version = Cpt(EpicsSignalRO, 'FirmwareVersion_RBV', kind='config')

    wait_for_plugins = Cpt(EpicsSignal, 'WaitForPlugins',
                           string=True, kind='config')
    

    def increment_number_in_string(self, s):
        # Find the last number in the string
        match = re.search(r'\d+$', s)
        if match:
            # Extract the number and increment it
            number = int(match.group())
            incremented_number = number + 1
            # Replace the old number with the incremented number
            new_s = re.sub(r'\d+$', str(incremented_number), s)
            return new_s
        else:
            # Return the original string if no number is found at the end
            return s


    def stage(self, *args, **kwargs):
        super().stage(*args, **kwargs)
        self.manual_trigger.set(1).wait(60)

        #read the current filename 
        # current_file_name = self.fw_name_pattern.get()
        # new_file_name = self.increment_number_in_string(current_file_name)
        # #increment by 1
        # self.fw_name_pattern.set(new_file_name)
        #arm
        def check_value(*, old_value, value, **kwargs):
            "Return True when the acquisition is complete, False otherwise."
            return (old_value == 0 and value == 1)

        status = SubscriptionStatus(self.armed, check_value, run=False)
        self.acquire.set(1)
        return status
    
    def unstage(self, *args, **kwargs):
        ret = super().unstage(*args, **kwargs)
        # disable manual trigger
        self.manual_trigger.set(0).wait(60)
        # disarm (by setting acq to 0)

        def check_value(*, old_value, value, **kwargs):
            "Return True when the acquisition is complete, False otherwise."
            return (old_value == 1 and value == 0)

        status = SubscriptionStatus(self.armed, check_value, run=False)

        self.acquire.set(0)
        return status

    def trigger(self, *args, **kwargs):

        def check_value(*, old_value, value, **kwargs):
            "Return True when the acquisition is complete, False otherwise."
            return (value - old_value == 1  )

        status = SubscriptionStatus(self.num_images_counter, check_value, run=False)
        
        self.special_trigger_button.set(1).wait(60)

        return status
    

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.stage_sigs['wait_for_plugins'] = 'Yes'
        #self.stage_sigs['trigger_mode'] = 3
        #self.stage_sigs['num_triggers'] = 1       

    def ensure_nonblocking(self):
        self.stage_sigs['wait_for_plugins'] = 'Yes'
        for c in self.parent.component_names:
            cpt = getattr(self.parent, c)
            if cpt is self:
                continue
            if hasattr(cpt, 'ensure_nonblocking'):
                cpt.ensure_nonblocking()

class TIFFPluginWithFileStore(TIFFPlugin, FileStoreTIFFIterativeWrite):
    ...


class EigerDetector(EigerDetector):
    
    cam = Cpt(EigerDetectorCamV33, 'cam1:')
    
    def trigger(self):
       
       # Call the cam trigger method (assumes that the device is already armed)
       return  self.cam.trigger()
        
    def stage(self, *args, **kwargs):
        return self.cam.stage(*args, **kwargs)
    
    def unstage(self, *args, **kwargs):
        return self.cam.unstage(*args, **kwargs)
    
    @property
    def hints(self):
        return {'fields': [self.stats1.total.name]}
    
    def set_acquisition_time(self, time):
        """The aquire time is in seconds"""
        self.cam.acquire_time.set(time)
        self.cam.acquire_period.set(time)

    def set_detector_distance(self, dist):
        """Distance in cm"""
        self.cam.det_distance.set(dist)

    def setup_eiger(self):
        self.cam.ensure_nonblocking()
        self.cam.trigger_mode.set(0) # this set it to internal series, EIGER_trigger_mode = ME[4]det
        self.cam.num_images.set(1) # EIGER_nimages = int(ME[5])
        self.cam.fw_num_images_per_file.set(1) 
        self.cam.compression_algo.set(1) # set compression method to bslz4
        self.cam.beam_center_y.set(1500)
        self.cam.beam_center_x.set(1500)

