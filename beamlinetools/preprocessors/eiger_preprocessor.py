from bluesky.preprocessors import SupplementalData, plan_mutator
 
def add_eiger_metadata(plan, dcm, RE):
    """
    Modifies a given plan to include a check that potentially repeats part of the plan based on the time since the last injection and a defined protection window. 

    This function wraps the original plan with a mutator that listens for specific conditions in the message stack. If the conditions are met (not a continuous scan, specific sequence of commands observed, and the time since the last injection is less than the protection window), it repeats a segment of the plan after waiting until the protection window has passed. This is intended to protect against too frequent injections by ensuring there's a minimum time interval between them.

    Args:
        plan (generator): The original plan to be executed, which yields messages.
        last_inj (object): An object that provides the time since the last injection, expected to have a `.get()` method that returns time in tenths of seconds.
        protection_window (int): The minimum allowed time (in seconds) between injections to enforce protection.

    Returns:
        generator: A new plan that includes the modified behavior to check for injection timing and potentially repeat part of the plan.

    Inner Function:
        repeat_trigger(msg, last_msg, msg_stack): A closure used to mutate the original plan. It checks the message stack and the time since the last injection against the protection window. If conditions are met, part of the plan is repeated; otherwise, the original plan continues.

    Note:
        - This function assumes the presence of a `plan_mutator` function that applies the `repeat_trigger` function to the original plan.
        - The inner workings, such as the specific conditions checked and the mechanism of repeating plan segments, depend on external functions not defined here.
    """ 

    def setup_eiger_param(msg):
        if msg.command == "stage":
            if msg.obj.name == "eiger":
                scan_id = RE.md["scan_id"]+1
                base_eiger = RE.md["base_eiger"]
                file_name = base_eiger+'_'+"{:06d}".format(scan_id)+'_'
                eiger_det = msg.obj
                en = dcm.p.energy.readback.get() * 1000 # convert in keV
                eiger_det.cam.photon_energy.set(en)
                eiger_det.cam.fw_name_pattern.set(file_name)
                return None, None
            return None, None
        else:
            return None, None
    plan1 = plan_mutator(plan, setup_eiger_param)
    return (yield from plan1)

class SupplemetalDataEiger(SupplementalData):
    """Supplemental data for for repeating measurements if an injection happened.


    Args:
            last_inj (Signal): The Ophyd signal connnected to the lastShot count
            protection_window (int or float): the time to wait after the injection to 
                                        repeat the measurement 

    Usage:

            from beamlinetools.preprocessor.avoid_injection import SupplemetalDataInjection
            repeat_trigger_injection = SupplemetalDataInjection(last_inj=last_inj, protection_window=2)
            RE.preprocessors.append(repeat_trigger_injection)

    """    

    def __init__(self, *args, dcm, RE, **kwargs):     
        super().__init__(*args, **kwargs)
        self.dcm = dcm
        self.RE=RE
        
    def __call__(self, plan):
        plan = add_eiger_metadata(plan, self.dcm, self.RE)
        return (yield from plan)


