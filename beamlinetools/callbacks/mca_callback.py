from beamlinetools.callbacks.csv_exporter import *

import os
import h5py


from bluesky.callbacks import CallbackBase

class McaCallback(CallbackBase):
    """Callback for the MCA detector.

    This callback save the mca data in the 
    correct Experiment/set folder.
    It runs only if in the detectors list there is 'mca'
    When the stop document is created the data from mca1 is saved
    We should decide how to include the other mca detectors,
    and how to deal with scans that move more than one motor 
    (at the moment the callback would simply fail)

    """
    def __init__(self, RE, db):
        self.RE = RE
        self.db = db
        self._mca_is_present = False

    def start(self, doc):

        try:
            self.motor_name = list(doc['motors'])
        except KeyError as e:
            # we set motor name to None, 
            # in case the plan was count and no motors are present
            self.motor_name=["None"]
            print("motor name not found")
            pass

        try:
            self.detectors  = list(doc['detectors'])
        except KeyError as e:
            print("detector not found")
            pass
        
        if 'mca' in self.detectors:
            self._mca_is_present = True
    
    def event(self, doc):
        """ We have to do what we do in stop here. Fine to save only the spectrum in channel 1
        
        We need scanNumber_pointNumber.
        
        """
        pass

    def stop(self, doc):
        if self._mca_is_present:
            print("found mca detector")
            # retrieve exported_data_dir folder
            exported_data_dir = self.RE.md['exported_data_dir']
            # retrieve base name
            base_name = self.RE.md['base_name']
            #create mca folder if does not exist
            mca_folder = os.path.join(exported_data_dir, 'mca')
            if not os.path.exists(mca_folder):
                print(f"Save folder {mca_folder}")
                os.makedirs(mca_folder)
            # retrieve scan ID and pad it with zeros (6 digits in total)
            scan_id = "{:06d}".format(self.RE.md['scan_id'])
            # construct new mcs filename
            mca_filename = os.path.join(mca_folder, base_name+'_'+scan_id+'.h5')
            
            run = self.db[-1]
            scan = run.primary.read()
          
            
            # fetch the data of first channel
            mca = scan['mca_spectrum1']
            
            # account for the case no motor was moved
            if self.motor_name[0] == "None":
                motor_pos = 0
            else:
                motor_pos = scan[self.motor_name[0]]
            
            with h5py.File(mca_filename, 'w') as hf:
                scan = hf.create_group('1D Scan')
                dset = hf.create_dataset("1D Scan/XRF data",  data=mca)
                dset.attrs.create('DATASET_TYPE', "Raw XRF counts")
                
                dset = hf.create_dataset("1D Scan/X Positions",  data=motor_pos)
                dset.attrs.create('DATASET_TYPE', "X")
                dset.attrs.create('Motor info', self.motor_name[0])
        else:
            pass
        self._mca_is_present = False