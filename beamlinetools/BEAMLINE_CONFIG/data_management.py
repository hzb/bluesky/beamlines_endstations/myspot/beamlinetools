
from beamlinetools.BEAMLINE_CONFIG.base import RE, db
from beamlinetools.BEAMLINE_CONFIG.beamline import dcm
from beamlinetools.callbacks.mca_callback import McaCallback
from beamlinetools.data_management.data_structure import MySpotDataStructure    


# MySpot Data structure folder
mds = MySpotDataStructure(RE)



# MCA Callback
mca_data_export_callabck = McaCallback(RE, db)
RE.subscribe(mca_data_export_callabck)


# eiger preprocessor
from beamlinetools.preprocessors.eiger_preprocessor import SupplemetalDataEiger
from .base import RE

sde = SupplemetalDataEiger(dcm=dcm, RE=RE)
RE.preprocessors.append(sde)