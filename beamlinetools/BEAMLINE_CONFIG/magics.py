from beamlinetools.magics.simplify_syntax import Simplify
from beamlinetools.magics.peakinfo import PeakInfoMagic
from bluesky.magics import BlueskyMagics
from beamlinetools.magics.standard_magics import BlueskyMagicsBessy
from IPython import get_ipython
from .base import RE, bec

## IMPORTANT : do not change the order of the follwing two lines. 
# standard magics
get_ipython().register_magics(BlueskyMagics)
# custom magics - it will override some standard magics
label_axis_dict = {
    "dcm": ["dcm.monoz.user_readback", 
                      "dcm.p.energy.readback",
                      "dcm.p.height.readback",
                      "dcm.p.cr2vetr.user_readback",
                      "dcm.p.cr2latr.user_readback",
                    #   "r.tttz.readback",
                    #   "r.ttrx.readback",
                    #   "r.ttry.readback",
                    #   "r.ttrz.readback",
                    #   "r.ps.det1.readback",
                    #   "r.ps.det2.readback",
                      ]
}
exclude_labels_from_wa=['detectors']
get_ipython().register_magics(BlueskyMagicsBessy(RE, get_ipython(), database_name ="db", exclude_labels_from_wa=exclude_labels_from_wa,label_axis_dict=label_axis_dict))

simplify = Simplify(get_ipython())
simplify.autogenerate_magics('/opt/bluesky/beamlinetools/beamlinetools/BEAMLINE_CONFIG/plans.py')
run_plan = simplify.execute_magic

get_ipython().register_magics(PeakInfoMagic)
# usage: peakinfo