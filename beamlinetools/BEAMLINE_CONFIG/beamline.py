# Set infinite connection timout and timoeut, workaround for bad network
from ophyd.signal import EpicsSignalBase
EpicsSignalBase.set_defaults(connection_timeout= None, timeout=None)

from ophyd.epics_motor import EpicsMotor

from beamlinetools.devices.accelerator import DetailedAccelerator, TopUpMonitoring
from beamlinetools.devices.ophydMFLibrary import keithley6485
from beamlinetools.devices.au import AUmySpot
from beamlinetools.devices.dcm_mySpot import DCMmySpot
from beamlinetools.devices.mca import MyEpicsMCA
from beamlinetools.devices.eiger import EigerDetector
# standard magics
from bluesky.magics import BlueskyMagics
get_ipython().register_magics(BlueskyMagics)


# simulated devices
from ophyd.sim import det1, det2, det3, det4, motor1, motor2, motor, noisy_det   
print('\n\n########################################')
print("Connecting to Devices")
# Ring
print("Connecting to Accelerator")
accelerator = DetailedAccelerator("", name="accelerator")
next_injection = TopUpMonitoring("", name="next_injection")
accelerator.wait_for_connection()
next_injection.wait_for_connection()

# Keithleys
print('Connecting to Amperometers')
ks = 'K64851MF102L:'
kth00    = keithley6485(ks + '12', name='myspot_keithley0', read_attrs=['readback'], labels={'detectors'})
kth00.wait_for_connection()
kth01    = keithley6485(ks + '14', name='myspot_keithley1', read_attrs=['readback'], labels={'detectors'})
kth01.wait_for_connection()

# motors
print('Connecting to motors')
hgx = EpicsMotor("DCM1OS2L:l0204007", name='hgx')
hgx.wait_for_connection()
mz = EpicsMotor("DCM1OS2L:l0206004", name='mz')
mz.wait_for_connection()

# Monochromator
print('Connecting to DCM')
dcm = DCMmySpot("DCM1OS2L:", name='dcm')
dcm.wait_for_connection()
dcm.p.set_mono_type()

# Apertures
print('Connecting to Slit3')
slit3 = AUmySpot("DCM1OS2L:", name='slit3')
slit3.wait_for_connection()

# Mirrors

# Detectors
print('Connecting to mca')
mca= MyEpicsMCA('dxpXMAP:', name="mca")
mca.wait_for_connection()

print('Connecting to EIGER')
prefix = 'EIG:'
eiger = EigerDetector(prefix, name="eiger")
eiger.wait_for_connection()
eiger.setup_eiger()
eiger.set_detector_distance(300)
eiger.set_acquisition_time(1)

print('Connection to all devices successful')
print('########################################')
print("\n\n")